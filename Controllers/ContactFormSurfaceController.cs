﻿using BCoTRestaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace BCoTRestaurant.Controllers
{
    public class ContactFormSurfaceController : SurfaceController
    {
        // GET: ContactFormSurface
        public ActionResult Index()
        {
            return PartialView("ContactForm", new ContactFormViewModel());
        }

        [HttpPost]
        public ActionResult HandleFormSubmit(ContactFormViewModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // send email
            string userMessage = string.Format("<div style='font-family: arial; font-size: 90%'>Tel: {0}<br />Number of diners: {1}<br />Date Time: {2}<br />{3}</div>", model.PhoneNumber, model.Diners, model.DateTime, model.Message);

            MailMessage message = new MailMessage();
            message.To.Add(model.EmailRecipient);

            if (model.CCRecipient != null)
            {
                message.To.Add(model.CCRecipient);
            }

            message.IsBodyHtml = true;
            message.Subject = model.Subject;
            message.From = new MailAddress(model.Email, model.Name);
            message.Body = userMessage;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(message);

            // send automated email to user
            MailMessage autoMessage = new MailMessage();
            autoMessage.IsBodyHtml = true;
            autoMessage.To.Add(model.Email);
            autoMessage.Subject = String.Format("RE: {0}", model.Subject);
            autoMessage.From = new MailAddress("restaurant@bcot.ac.uk", "BCoT Restaurant");
            autoMessage.Body = model.AutoMessage;
            SmtpClient autoSmtp = new SmtpClient();
            autoSmtp.Send(autoMessage);

            TempData["success"] = true;

            return RedirectToCurrentUmbracoPage();
        }
    }
}