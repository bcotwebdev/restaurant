  <!-- Navigation Menu Javascript -->
    var $oe_menu        = $('#oe_menu');
    var $oe_menu_items  = $oe_menu.children('li');
    var $oe_overlay     = $('#oe_overlay');

    if(screen.width > 480)
    {
      $oe_menu_items.bind('mouseenter',function(){
        var $this = $(this);
        $this.addClass('slided selected');
        $this.children('div').css('z-index','9999').stop(true,true).slideDown(200,function(){
          $oe_menu_items.not('.slided').children('div').hide();
          $this.removeClass('slided');
        });
      }).bind('mouseleave',function(){
        var $this = $(this);
        $this.removeClass('selected').children('div').css('z-index','1');
        $this.children('div').hide();  
      });
    }
    else
    {
      $oe_menu_items.bind('click', function(){
        var $this = $(this);
        if($this.hasClass('selected'))
        {
          $this.removeClass('selected').children('div').css('z-index','1');
          $oe_menu.removeClass('hovered');
          $this.children('div').hide();
        }
        else
        {
          $this.addClass('slided selected');
          $this.children('div').css('z-index','9999').stop(true,true).slideDown(200,function(){
            $oe_menu_items.not('.slided').children('div').hide();
            $this.removeClass('slided');
          });
          $oe_menu.addClass('hovered');
        }
      }).bind('mouseleave', function(){
        var $this = $(this);
        if($this.hasClass('selected'))
        {
          $this.removeClass('selected').children('div').css('z-index','1');
        }
      });
    }