$(document).ready(function(){
  $('.mainSlider').slick({
	  slidesToShow: 1,
	  dots: true,
	  infinite: true,
	  arrows: true,
	  centerMode: true,
	  variableWidth: true,
	  autoplay: true
  });
});