﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TheCogworksCookieRefresher.ascx.cs" Inherits="TheCogworksCookieDirective.Web.usercontrols.TheCogworksCookieDirective.CookieRefresher" %>
<%--
Created by The Cogworks
Website: thecogworks.co.uk

Created 29 June 2012
--%>

<asp:Button ID="BtnRefresh" runat="server" Text="Refresh Cookies" 
    onclick="BtnRefresh_Click" /> 
    
<div ID="divDone" runat="server" Visible="false" style="color:Green; display:inline-block;">Done</div>
