﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCoTRestaurant.Models
{
    public class ContactFormViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required, EmailAddressAttribute]
        public string Email { get; set; }

        [Phone]
        public string PhoneNumber { get; set; }

        [Required, DataType(DataType.DateTime)]
        public string DateTime { get; set; }

        [Required, Range(1, 20)]
        public string Diners { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Message { get; set; }

        public string EmailRecipient { get; set; }

        public string CCRecipient { get; set; }

        [AllowHtml]
        public string AutoMessage { get; set; }
    }
}
